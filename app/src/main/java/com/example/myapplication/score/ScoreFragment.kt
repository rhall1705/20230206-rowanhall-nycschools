package com.example.myapplication.score

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.myapplication.MainActivity
import com.example.myapplication.R
import com.example.myapplication.network.Resource
import com.example.myapplication.databinding.FragmentScoreBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by Rowan Hall
 */

@AndroidEntryPoint
class ScoreFragment: Fragment() {

    private val viewModel by viewModels<ScoreViewModel>()
    private var binding: FragmentScoreBinding? = null
    private val args by navArgs<ScoreFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.liveData().observe(this) {
            bind(it)
        }
        if (savedInstanceState == null) {
            // Perform initial fetch on first creation only
            getScores()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentScoreBinding.inflate(inflater, container, false)
        this.binding = binding
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name = args.name ?: getString(R.string.scores_default_title)
        (requireActivity() as MainActivity).supportActionBar?.title = name
        binding?.srlScores?.setOnRefreshListener {
            getScores()
        }
        viewModel.liveData().value?.let {
            bind(it)
        }
    }

    private fun getScores() {
        val dbn = args.dbn ?: {
            Log.e(TAG, "null navigation args")
            Toast.makeText(context, R.string.generic_error, Toast.LENGTH_SHORT).show()
        }
        viewModel.getScores(dbn as String)
    }

    private fun bind(resource: Resource<ScoreViewState>) {
        when (resource) {
            is Resource.Success -> {
                binding?.apply {
                    srlScores.isRefreshing = false
                    val data = resource.data ?: return
                    if (data.dbn == null) {
                        tvEmpty.visibility = View.VISIBLE
                    } else {
                        tvEmpty.visibility = View.GONE
                        tvNumTestTakers.text = getString(R.string.scores_num_test_takers, data.testTakers)
                        tvMathAverage.text = getString(R.string.scores_math, data.mathAverage)
                        tvReadingAverage.text = getString(R.string.scores_reading, data.readingAverage)
                        tvWritingAverage.text = getString(R.string.scores_writing, data.writingAverage)
                    }
                }
            }
            is Resource.Loading -> {
                binding?.srlScores?.isRefreshing = true
            }
            is Resource.Error -> {
                binding?.apply {
                    srlScores.isRefreshing = false
                    tvEmpty.visibility = View.GONE
                }
                binding?.srlScores?.isRefreshing = false
                Log.e(TAG, resource.message ?: "No error message")
                val displayError = resource.message ?: getString(R.string.generic_error)
                Toast.makeText(context, displayError, Toast.LENGTH_SHORT).show()
            }
        }
    }
}

private const val TAG = "ScoreFragment"