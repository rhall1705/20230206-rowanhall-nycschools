package com.example.myapplication.score

import com.example.myapplication.network.Resource
import com.example.myapplication.network.BaseNetworkRepository
import com.example.myapplication.network.CityOfNewYorkService
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

/**
 * Created by Rowan Hall
 */

@ActivityRetainedScoped
class ScoreRepository @Inject constructor(private val conyService: CityOfNewYorkService) :
    BaseNetworkRepository() {

    suspend fun getScoresForSchool(dbn: String): Flow<Resource<ScoreViewState>> {
        return flow {
            when (val scoreResponse = safeApiCall { conyService.getScores(dbn) }) {
                is Resource.Success -> {
                    val data = scoreResponse.data
                    if (data == null || data.isEmpty()) {
                        emit(Resource.Success(ScoreViewState(null, "", "", "", "")))
                        return@flow
                    }
                    val score = data[0]
                    emit(
                        Resource.Success(
                            ScoreViewState(
                                score.dbn,
                                score.num_of_sat_test_takers,
                                score.sat_critical_reading_avg_score,
                                score.sat_math_avg_score,
                                score.sat_writing_avg_score
                            )
                        )
                    )
                }
                is Resource.Loading -> {
                    emit(Resource.Loading())
                }
                is Resource.Error -> {
                    emit(Resource.Error(scoreResponse.message ?: ""))
                }
            }
        }
            .onStart { emit(Resource.Loading()) }
            .flowOn(Dispatchers.IO)
    }
}