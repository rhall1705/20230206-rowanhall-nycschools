package com.example.myapplication.score

/**
 * Created by Rowan Hall
 *
 * Represents the data backing a scores view so it can be bound to by [ScoreFragment]
 *
 * While this is identical to the response object, a more complex application might map those fields
 * to values better formatted for user-facing views, so we create a separation.
 */

data class ScoreViewState(val dbn: String?,
                          val testTakers: String,
                          val readingAverage: String,
                          val mathAverage: String,
                          val writingAverage: String)