package com.example.myapplication.score

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.myapplication.network.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Rowan Hall
 */
@HiltViewModel
class ScoreViewModel @Inject constructor(
    application: Application,
    private val repository: ScoreRepository
) : AndroidViewModel(application) {

    private val liveData: MutableLiveData<Resource<ScoreViewState>> = MutableLiveData()

    fun getScores(dbn: String) {
        viewModelScope.launch {
            repository.getScoresForSchool(dbn)
                .collect {
                    liveData.value = it
                }
        }
    }

    fun liveData(): LiveData<Resource<ScoreViewState>> = liveData
}