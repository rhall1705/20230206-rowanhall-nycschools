package com.example.myapplication

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Rowan Hall
 */
@HiltAndroidApp
class NycSchoolsApplication: Application()