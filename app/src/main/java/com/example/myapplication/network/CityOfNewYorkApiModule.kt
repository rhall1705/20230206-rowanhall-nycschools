package com.example.myapplication.network

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Rowan Hall
 *
 * Hilt module to provide dependencies for CONY web service, including intercepting requests to
 * apply app token in header
 */
@Module
@InstallIn(SingletonComponent::class)
object CityOfNewYorkApiModule {

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor {
            val newRequest = it
                .request()
                .newBuilder()
                .header("X-App-Token", "zU6A2iqJjAj2yPAvybaiWGMlm")
                .build()
            it.proceed(newRequest)
        }.build()
    }

    @Singleton
    @Provides
    fun provideGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()

    @Singleton
    @Provides
    fun provideJsonService(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): CityOfNewYorkService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()
        return retrofit.create(CityOfNewYorkService::class.java)
    }
}

const val BASE_URL = "https://data.cityofnewyork.us"