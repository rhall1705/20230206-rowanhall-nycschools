package com.example.myapplication.network

/**
 * Created by Rowan Hall
 *
 * Sealed class to allow for switch statements that map request status to UI state
 */
sealed class Resource<T>(
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T) : Resource<T>(data)
    class Error<T>(message: String, data: T? = null) : Resource<T>(data, message)
    class Loading<T> : Resource<T>()
}