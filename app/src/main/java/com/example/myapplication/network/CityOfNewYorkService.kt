package com.example.myapplication.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Rowan Hall
 *
 * CONY web service endpoints
 */
interface CityOfNewYorkService {

    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<SchoolData>>

    @GET("/resource/f9bf-2cp4.json")
    suspend fun getScores(@Query("dbn") dbn: String): Response<List<ScoreData>>
}

data class SchoolData(
    val dbn: String,
    val school_name: String,
    val overview_paragraph: String,
    val website: String
)

data class ScoreData(
    val dbn: String,
    val school_name: String,
    val num_of_sat_test_takers: String,
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String,
    val sat_writing_avg_score: String
)