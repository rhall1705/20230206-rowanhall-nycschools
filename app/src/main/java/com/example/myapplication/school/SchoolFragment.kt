package com.example.myapplication.school

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.R
import com.example.myapplication.network.Resource
import com.example.myapplication.databinding.FragmentSchoolBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by Rowan Hall
 */

@AndroidEntryPoint
class SchoolFragment : Fragment(), ItemClickListener {

    private val viewModel: SchoolViewModel by viewModels()
    private var binding: FragmentSchoolBinding? = null
    private val adapter = SchoolAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.liveData().observe(this) {
            when (it) {
                is Resource.Success -> {
                    binding?.srlSchools?.isRefreshing = false
                    val data = it.data?.items ?: listOf()
                    adapter.submitList(data)
                    binding?.tvEmpty?.visibility = if (data.isEmpty()) View.VISIBLE else View.GONE
                }
                is Resource.Loading -> {
                    binding?.srlSchools?.isRefreshing = true
                }
                is Resource.Error -> {
                    binding?.apply {
                        srlSchools.isRefreshing = false
                        tvEmpty.visibility = View.GONE
                    }
                    Log.e(TAG, it.message ?: "No error message")
                    val displayError = it.message ?: getString(R.string.generic_error)
                    Toast.makeText(context, displayError, Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (savedInstanceState == null) {
            // Perform initial fetch on first creation only
            viewModel.getSchools()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentSchoolBinding.inflate(inflater, container, false)
        this.binding = binding
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            val layoutManager = LinearLayoutManager(context)
            rvSchools.layoutManager = layoutManager
            rvSchools.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))
            rvSchools.adapter = adapter
            srlSchools.setOnRefreshListener {
                viewModel.getSchools()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        this.binding = null
    }

    override fun onScoresButtonClicked(viewState: SchoolItemViewState) {
        findNavController().navigate(SchoolFragmentDirections.actionSchoolFragmentToScoreFragment(viewState.dbn, viewState.name))
    }
}

private const val TAG = "SchoolFragment"