package com.example.myapplication.school

import com.example.myapplication.network.Resource
import com.example.myapplication.network.BaseNetworkRepository
import com.example.myapplication.network.CityOfNewYorkService
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

/**
 * Created by Rowan Hall
 */

@ActivityRetainedScoped
class SchoolRepository @Inject constructor(private val conyService: CityOfNewYorkService) :
    BaseNetworkRepository() {

    suspend fun getSchools(): Flow<Resource<SchoolViewState>> {
        return flow {
            when (val schoolResponse = safeApiCall { conyService.getSchools() }) {
                is Resource.Success -> {
                    val data = schoolResponse.data!!
                    val itemViewStates = data.map {
                        SchoolItemViewState(it.dbn, it.school_name, it.overview_paragraph, it.website)
                    }
                    emit(Resource.Success(SchoolViewState(itemViewStates)))
                }
                is Resource.Loading -> {
                    emit(Resource.Loading())
                }
                is Resource.Error -> {
                    emit(Resource.Error(schoolResponse.message ?: ""))
                }
            }
        }
            .onStart { emit(Resource.Loading()) }
            .flowOn(Dispatchers.IO)
    }
}