package com.example.myapplication.school

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.myapplication.network.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Rowan Hall
 */

@HiltViewModel
class SchoolViewModel @Inject constructor(
    application: Application,
    private val repository: SchoolRepository
) : AndroidViewModel(application) {

    private val liveData: MutableLiveData<Resource<SchoolViewState>> = MutableLiveData()

    fun getSchools() {
        viewModelScope.launch {
            repository.getSchools()
                .collect {
                    liveData.value = it
                }
        }
    }

    fun liveData(): LiveData<Resource<SchoolViewState>> = liveData
}