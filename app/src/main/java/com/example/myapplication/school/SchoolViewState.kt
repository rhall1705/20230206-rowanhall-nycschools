package com.example.myapplication.school

/**
 * Created by Rowan Hall
 *
 * Represents the data backing a schools view so it can be bound to by [SchoolFragment]
 *
 * While this is identical to the response object, a more complex application might map those fields
 * to values better formatted for user-facing views, so we create a separation.
 */

data class SchoolViewState(val items: List<SchoolItemViewState>)
data class SchoolItemViewState(val dbn: String, val name: String, val overview: String, val website: String)
