package com.example.myapplication.school

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

/**
 * Created by Rowan Hall
 *
 * ListAdapter to display a list of schools. Subclassing ListAdapter provides free animations for
 * removal, modification, etc. of the backing dataset, although in this app the only use case is
 * if swipe to refresh picks up different schools than the previous result.
 */

class SchoolAdapter(private val clickListener: ItemClickListener):
    ListAdapter<SchoolItemViewState, SchoolViewHolder>(SchoolDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        return SchoolViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_view_school, parent, false)
        )
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    override fun onViewRecycled(holder: SchoolViewHolder) {
        holder.recycle()
        super.onViewRecycled(holder)
    }

}

class SchoolDiffCallback : DiffUtil.ItemCallback<SchoolItemViewState>() {
    override fun areItemsTheSame(
        oldItem: SchoolItemViewState,
        newItem: SchoolItemViewState
    ): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(
        oldItem: SchoolItemViewState,
        newItem: SchoolItemViewState
    ): Boolean {
        return oldItem.name == newItem.name &&
                oldItem.overview == newItem.overview &&
                oldItem.website == newItem.website
    }

}

class SchoolViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val nameView = itemView.findViewById<TextView>(R.id.tv_school_name)
    private val descriptionView = itemView.findViewById<TextView>(R.id.tv_overview)
    private val websiteView = itemView.findViewById<TextView>(R.id.tv_website)
    private val scoresView = itemView.findViewById<Button>(R.id.btn_scores)

    fun bind(viewState: SchoolItemViewState, clickListener: ItemClickListener) {
        nameView.text = viewState.name
        descriptionView.text = viewState.overview
        websiteView.text = viewState.website
        scoresView.setOnClickListener {
            clickListener.onScoresButtonClicked(viewState)
        }
    }

    fun recycle() {
        scoresView.setOnClickListener(null)
    }
}

interface ItemClickListener {
    fun onScoresButtonClicked(viewState: SchoolItemViewState)
}
