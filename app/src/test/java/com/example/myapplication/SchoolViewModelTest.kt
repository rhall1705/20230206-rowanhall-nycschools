package com.example.myapplication

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.myapplication.network.Resource
import com.example.myapplication.school.SchoolItemViewState
import com.example.myapplication.school.SchoolRepository
import com.example.myapplication.school.SchoolViewModel
import com.example.myapplication.school.SchoolViewState
import com.example.myapplication.score.ScoreViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

/**
 * Sample unit test to confirm [SchoolViewModel] properly collects emitted values from its
 * repository to its LiveData instance that is observed by its view. Given more time, additional
 * unit testing would include similar tests for:
 *
 * - progress/failure cases in this test
 * - success/progress/failure cases for [ScoreViewModel]
 * - both repository classes ensuring a mocked value from their retrofit service is emitted from
 * their flow methods.
 *
 * It would *not* include tests around view classes, as these would best be handled by instrumented/
 * automated tests.
 */
@OptIn(ExperimentalCoroutinesApi::class)
class SchoolViewModelTest {

    private val repository = mockk<SchoolRepository>(relaxed = true)
    @Suppress("DEPRECATION")
    private val dispatcher = TestCoroutineDispatcher()
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
    }

    @Test
    fun testSuccess() = runTest {
        val viewModel = SchoolViewModel(mockk(), repository)
        val successResource = Resource.Success(SchoolViewState(listOf(SchoolItemViewState("testDbn", "testName", "testOverview", "testWebsite"))))
        val testFlow = flow {
            emit(successResource)
        }
        coEvery {
            repository.getSchools()
        } returns testFlow
        runBlocking {
            // LiveData must be observed to emit values
            viewModel.liveData().observeForever {  }
            viewModel.getSchools()
        }
        assert(viewModel.liveData().value == successResource)
    }
}